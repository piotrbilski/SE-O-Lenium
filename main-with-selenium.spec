# -*- mode: python -*-

import selenium
selenium_tree = Tree(str(selenium.__path__[0]), prefix="selenium")



a = Analysis(['main.py'],
             pathex=['C:\\SE-O-Lenium'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='SE-O-Lenium.exe',
          icon='icon.ico',
          debug=False,
          strip=None,
          upx=True,
          console=False )

coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               selenium_tree, # include selenium_tree resource
               strip=None,
               upx=True,
               console=False,
               name='SE-O-Lenium')

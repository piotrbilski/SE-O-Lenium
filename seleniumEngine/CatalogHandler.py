from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.common.exceptions import NoSuchWindowException
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.options import Options



import json

class LackOfTextException(Exception): pass


class CatalogHandler(object):

    acceptable_engines = ( webdriver.Firefox,webdriver.Opera)

    form_map = {
        "email" : ("email","mail","e-mail","wpis_email"),
        "keywords": ("keywords","key-words","wpis_tags"),
        "www":("site","www","wpis_url"),
        "multikod_select":("groups","group"),
        "multikod":("code","codes","multicode","multikod","wpis_code"),
        "texts":("description","desc","opis"),
        "titles":("title","page_title","title_page","wpis_title"),
        "main" : ("site_cat_",),
        "sub" : ("site_sub_",),
        "submit" :("code","codes","multicode","multikod","wpis_code")

    }


    def __init__(self):

        self.webdriver = None
        self.load_config()

        # testing
        caps = DesiredCapabilities().FIREFOX
        #caps["pageLoadStrategy"] = "normal"  #  complete
        caps["pageLoadStrategy"] = "eager"  # interactive
        #caps["pageLoadStrategy"] = "none"   #  undefine


        for engine in self.acceptable_engines:
            try:

                ##self.webdriver = engine(capabilities=caps)

                self.webdriver = engine(capabilities=caps)

                #self.webdriver.implicitly_wait(1)
                return
            except Exception as ex:
                raise
                print ("{} is not available".format(engine))

    def load_config(self,filename = 'config.json'):
        try:
            with open(filename) as config_file:
                self.form_map = json.load(config_file)
        except Exception as ex:
            pass



    def openNewTab(self, page):
        try:
            driver = self.webdriver
            driver.switch_to.window(driver.window_handles[0])
            driver.execute_script("window.open('{}','_blank', '');".format(page))

        except Exception as ex:
            self.__init__()
            self.openNewTab(page)

    def fetch_category(self, category):
        if category == 'main':
            return self.__fetch_main_categories()
        elif category == 'sub':
            return self.__fetch_sub_categories()


    def __fetch_main_categories(self):
        categories = set()
        for window in self.window_list():
            self.webdriver.switch_to.window(window)
            try:
                select = Select(self.webdriver.find_element_by_id('site_cat_0')) #TODO: more common solution
                cat = { x.text for x in select.options }
                categories = categories | cat

            except:
                pass

        return categories

    def __fetch_sub_categories(self):
        categories = set()
        for window in self.window_list():
            self.webdriver.switch_to.window(window)
            try:
                candidates = self.webdriver.find_elements_by_css_selector('select[id^=site_sub_]')
                selects = [ Select(x) for x in candidates ]

                for select in selects:
                    cat = { x.text for x in select.options }
                    categories = categories | cat

            except Exception as ex:
                raise

        return categories


    def window_list(self):
        return self.webdriver.window_handles


    def switch_window(self, id):
        self.webdriver.switch_to.window(id)

    
    def find_form(self, type):

        driver = self.webdriver
        names = self.form_map.get(type)
        element = None
        
        for name in names:
            try:

                elements = driver.find_elements_by_name(name)
                for element in elements:
                    if element.tag_name in ("input","textarea","submit"):
                        return element

            except Exception as ex:
                pass

        return element

    def find_forms(self, type):

        driver = self.webdriver
        names = self.form_map.get(type)

        for name in names:
            try:
                elements = driver.find_elements_by_css_selector("[id^={}]".format(name))
                true_elements = [x for x in elements if x.tag_name in ('select', 'input')]
                return true_elements
            except Exception as ex:
                raise


    def fill_windows(self, enter = False, **kwargs):

        returned_texts = []
        for window in self.window_list():
            self.switch_window(window)
            for key in kwargs.keys():
                if key == 'sub' and isinstance( kwargs[key], list):
                   returned_texts = self._fill_multiple_fields_comb(key, kwargs[key])
                elif key != 'texts' and isinstance( kwargs[key], list): ### must be fixed in appropriate way
                   returned_texts = self._fill_multiple_fields(key, kwargs[key])
                else:
                   returned_texts = self._fill_single_field(key, kwargs[key])

        return returned_texts

    def _fill_multiple_fields(self, name, values):
        pop_form = True
        returned_texts = []
        form_fields = self.find_forms(name)
        form_fields.reverse()
        if form_fields:
            form_field = None
            for value in values:
                try:

                    form_field = form_fields.pop() if pop_form else form_field
                    handler = getattr(self, name, self.fill_form)
                    out = handler(element = form_field, text=value)
                    if out:
                        returned_texts.append(out)
                    pop_form = True
                except IndexError:
                    break
                except NoSuchElementException:
                    pop_form = False
                except Exception as ex:
                    raise

        return returned_texts

    def _fill_multiple_fields_comb(self, name, values):
        returned_texts = []
        form_fields = self.find_forms(name)
        form_fields.reverse()
        if form_fields:
            for form in form_fields:
                for value in values:
                    try:
                        handler = getattr(self, name, self.fill_form)
                        out = handler(element=form, text=value)
                        if out:
                            returned_texts.append(out)
                            break
                    except Exception as ex:
                        print(ex)


        return returned_texts


    def _fill_single_field(self, name, value):
        returned_texts = ''
        form_field = self.find_form(name)
        if form_field:
            try:
                handler = getattr(self, name, self.fill_form)
                out = handler(element=form_field, text=value)
                if out:
                    returned_texts = out
            except Exception as ex:
                print(ex)

        return returned_texts

    def fill_form(self,element=None, text='', enter = False):

        handler = getattr(self, "{}_handler".format(element.tag_name), self.default_handler())
        return handler(element, text, enter)

    def www(self, element = None, text='', enter = True):
        self.fill_form(element = element, text = text, enter = enter)

    def submit(self, element =None, text='', enter = True):
        self.fill_form(element=element, text=text, enter=enter)

    def texts(self,element=None, text='', enter = False):
        try:
            self.fill_form(element = element, text = text.pop())
            return text
        except IndexError as ex:
            print(ex)
            return []


    def titles(self,element=None, text='', enter = False):

        self.fill_form(element = element, text = next(text))


    def select_handler(self, element, text, enter = False):

        select_form = Select(element)
        try:
            select_form.select_by_visible_text(text)
            return text
        except Exception as ex:
            print(ex)
            return self.__js_select_handler(element, text)

    def __js_select_handler(self, element, text, enter = False):
        print("Need to use JS handler")
        element_id = element.get_attribute('id')
        for option in element.find_elements_by_tag_name('option'):
            if option.text == text:

                value = option.get_attribute('value') or ''
                js_code = "arguments[0].value={0};".format(value)
                self.webdriver.execute_script(js_code, element)
                js_code = 'var evt = new Event("change"); arguments[0].dispatchEvent(evt);'
                self.webdriver.execute_script(js_code, element)
                return text
        return None

    def input_handler(self,element, text, enter=False):

        if text:
            self.webdriver.execute_script('arguments[0].value = "{}"'.format(text), element)
        if enter:
            try:
                element.send_keys(Keys.ENTER)
            except:
                pass

    def textarea_handler(self, element, text, enter = False):

        self.webdriver.execute_script('arguments[0].value = "{}"'.format(text.replace('"', "'")), element)


    def close_windows(self):
        for idx, window in enumerate(self.window_list()):
            if idx == 0: continue
            try:
                self.switch_window(window)
                self.webdriver.close()
            except Exception as ex:
                print(ex)


    def default_handler(self,*args,**kwargs):
        pass

    def close(self):
        self.webdriver.quit()



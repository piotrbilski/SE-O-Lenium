#-*- coding: utf-8 -*-
from PySide import QtGui
from ui import mainWindow
from seleniumEngine import CatalogHandler
import random
import sys
import urllib.parse
from itertools import cycle
import threading

"""
    SEO-O-Lenium
    Author: Piotr Bilski


"""

def exception_handler(Exception):
    QMessage = QtGui.QMessageBox()
    QMessage.setWindowTitle(Exception.__class__.__name__)
    QMessage.setText("Wystapil nieoczekiwany blad, wiecej informacji znajdziesz ponizej")
    QMessage.setDetailedText(Exception.message)
    QMessage.exec_()



class MainWindow(mainWindow.Ui_MainWindow, QtGui.QMainWindow):

    main_categories = {}
    sub_categories = {}

    def __init__(self, catalogHandler = None, *args, **kwargs):
        self.catalogHandler = catalogHandler
        super(MainWindow, self).__init__(*args, **kwargs)
        self.initUI()


    def initUI(self):

        self.setupUi(self)

        self.QCatalogsOpen.clicked.connect(self.open_catalogs)
        self.QMailFill.clicked.connect(self.fill_mail)
        self.QwwwFill.clicked.connect(self.fill_www)
        self.QMultikodFill.clicked.connect(self.fill_multikod)
        self.QKeywordsFill.clicked.connect(self.fill_keywords)
        self.QTitlesFill.clicked.connect(self.fill_titles)
        self.QKeywordsFill.clicked.connect(self.fill_keywords)
        self.QTextsFill.clicked.connect(self.fill_texts)
        self.QSubmitAll.clicked.connect(self.fill_all)
        self.QcleanAll.clicked.connect(self.clean_all)
        self.QCloseAll.clicked.connect(self.close_all)

        self.QFetchCategories1.clicked.connect(lambda: self.fetch_categories('main'))
        self.QFetchCategories2.clicked.connect(lambda: self.fetch_categories('sub'))

        self.QCategoryFilter1.textChanged.connect(lambda text: self.filter_category(text, 'main'))
        self.QSearchCategories1.clicked.connect(lambda: self.check_categories('main'))

        self.QCategoryFilter2.textChanged.connect(lambda text: self.filter_category(text, 'sub'))
        self.QSearchCategories2.clicked.connect(lambda: self.check_categories('sub'))
        self.QSubmit.clicked.connect(self.submit_form)
        self.show()



    def open_catalogs(self):

        for page in self.__prepare_catalogs():
           self.catalogHandler.openNewTab(page)

    def __prepare_catalogs(self):
        catalogs_string = self.QCatalogs.toPlainText()
        catalogs = [catalog.strip() for catalog in catalogs_string.split('\n') if cat]

        return catalogs

    def __prepare_texts(self):
        texts_string = self.QTexts.toPlainText()
        texts_array = [ text.strip() for text in texts_string.split('\n') if text and len(text) > 200]

        return texts_array

    def __prepare_keywords(self):
        keywords_string = self.QKeywords.toPlainText()
        keywords =", ".join([keyword.strip() for keyword in keywords_string.split('\n') if keyword])
        return keywords

    def __prepare_titles(self, website_title=True):
        website = urllib.parse.urlparse(self.Qwww.text())

        websites = (
                    '{}{}{}'.format(website.scheme, "://", website.netloc),
                    website.netloc,
                    ''
                    )

        connectors = (' ', ' - ', ' | ', ' : ')

        titles_string = self.QTitles.toPlainText()
        titles = cycle((title.strip() for title in titles_string.split('\n') if title))

        new_titles = []

        window_list = self.catalogHandler.window_list()
        for windows in window_list:

            x, url, connector = random.choice((True, False)), random.choice(websites), random.choice(connectors)

            try:
                title = next(titles)
            except Exception: #TODO iteration exception
                title = ''

            url = url if website_title else ''
            x = x if website_title else False
            result_title = "%s%s%s%s%s" % (x*url, bool(url)*(x*connector), title, bool(url)*((not x)*connector), (not x)*url)
            new_titles.append(result_title)

        cyc_titles = cycle(new_titles)

        return cyc_titles

    def fetch_categories(self, category_type):
        categories = self.catalogHandler.fetch_category(category_type)
        layout_name = "_".join(("QLayout", category_type))
        dict_name = "_".join((category_type, 'categories'))
        category_dict = getattr(self, dict_name);
        layout = getattr(self, layout_name)
        category_dict.clear()
        self.__clear_layout(layout)
        sorted_categories = list(categories)
        sorted_categories.sort()
        for category in sorted_categories:
            check = QtGui.QCheckBox(category)
            category_dict[category] = check
            layout.addWidget(check)



    def __clear_layout(self, layout):
        try:
            while layout.count():
                child = layout.takeAt(0)
                if child.widget():
                    child.widget().deleteLater()
        except:
            pass

    def filter_category(self, text, category_type):

        dict_name = "_".join((category_type, 'categories'))
        category_dict = getattr(self, dict_name)

        if text == '':
            [y.show() for x, y in category_dict.items()]
        [ y.hide() for x, y in category_dict.items() ]
        [ y.show() for x, y in category_dict.items() if text.lower() in x.lower() ]

    def check_categories(self, category_type): #main or sub
        dict_name = "_".join((category_type, 'categories'))
        category_dict = getattr(self, dict_name)
        categories = [x for x, y in category_dict.items() if y.isChecked() ]
        self.catalogHandler.fill_windows(**{category_type : categories})

    def fill_mail(self):
        mail = self.QMail.text()
        self.catalogHandler.fill_windows(email = mail)

    def fill_www(self):

        www = self.Qwww.text()
        self.catalogHandler.fill_windows(www = www)

    def fill_multikod(self):

        multikod = self.QMultikod.text()
        multikod_select = self.QMultikodOption.text()
        self.catalogHandler.fill_windows(multikod_select = multikod_select, multikod = multikod)

    def fill_keywords(self):

        keywords = self.__prepare_keywords()
        self.catalogHandler.fill_windows(keywords = keywords)

    def fill_titles(self):
        title_website = self.QTitleWebsite.isChecked()
        cyc_titles = self.__prepare_titles(title_website)
        self.catalogHandler.fill_windows(titles = cyc_titles)


    def fill_texts(self):
        texts = self.__prepare_texts()
        returned_texts = self.catalogHandler.fill_windows(texts = texts)

        self.__set_returned_texts(returned_texts)

    def fill_all(self):
        mail = self.QMail.text()
        multikod = self.QMultikod.text()
        multikod_select = self.QMultikodOption.text()
        keywords = self.__prepare_keywords()
        title_website = self.QTitleWebsite.isChecked()
        cyc_titles = self.__prepare_titles(title_website)
        texts = self.__prepare_texts()
        returned_texts = self.catalogHandler.fill_windows(texts=texts, titles=cyc_titles, keywords=keywords, multikod_select=multikod_select, multikod=multikod, email=mail)
        self.__set_returned_texts(returned_texts)


    def submit_form(self):
        self.catalogHandler.fill_windows(submit='')

    def clean_all(self):

        self.Qwww.clear()
        self.QMultikod.clear()
        self.QKeywords.clear()
        self.QTitles.clear()
        self.QTexts.clear()


    def close_all(self):

        self.catalogHandler.close_windows()

    def __set_returned_texts(self, text_array):
        if text_array:
            self.QTexts.setPlainText("\n\nTekst\n\n".join(text_array))
        else:
            self.QTexts.setPlainText('')


    def closeEvent(self, *args, **kwargs):
        self.catalogHandler.close()
        super(MainWindow,self).closeEvent(*args, **kwargs)

if __name__ == "__main__":


    cat = CatalogHandler.CatalogHandler()
    app = QtGui.QApplication(sys.argv)
    window = MainWindow(cat)

    window.show()


    sys.exit(app.exec_())

# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created: Sat Oct 21 16:31:54 2017
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(662, 551)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../icon.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.tabWidget = QtGui.QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtGui.QWidget()
        self.tab.setObjectName("tab")
        self.gridLayout_2 = QtGui.QGridLayout(self.tab)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.groupBox_2 = QtGui.QGroupBox(self.tab)
        self.groupBox_2.setObjectName("groupBox_2")
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.groupBox_2)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.QwwwFill = QtGui.QPushButton(self.groupBox_2)
        self.QwwwFill.setObjectName("QwwwFill")
        self.horizontalLayout_2.addWidget(self.QwwwFill)
        self.Qwww = QtGui.QLineEdit(self.groupBox_2)
        self.Qwww.setObjectName("Qwww")
        self.horizontalLayout_2.addWidget(self.Qwww)
        self.gridLayout_2.addWidget(self.groupBox_2, 0, 0, 1, 3)
        self.groupBox_5 = QtGui.QGroupBox(self.tab)
        self.groupBox_5.setObjectName("groupBox_5")
        self.horizontalLayout_3 = QtGui.QHBoxLayout(self.groupBox_5)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.QMailFill = QtGui.QPushButton(self.groupBox_5)
        self.QMailFill.setObjectName("QMailFill")
        self.horizontalLayout_3.addWidget(self.QMailFill)
        self.QMail = QtGui.QLineEdit(self.groupBox_5)
        self.QMail.setObjectName("QMail")
        self.horizontalLayout_3.addWidget(self.QMail)
        self.gridLayout_2.addWidget(self.groupBox_5, 1, 0, 1, 3)
        self.groupBox_6 = QtGui.QGroupBox(self.tab)
        self.groupBox_6.setObjectName("groupBox_6")
        self.horizontalLayout_4 = QtGui.QHBoxLayout(self.groupBox_6)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.QMultikodFill = QtGui.QPushButton(self.groupBox_6)
        self.QMultikodFill.setObjectName("QMultikodFill")
        self.horizontalLayout_4.addWidget(self.QMultikodFill)
        self.QMultikod = QtGui.QLineEdit(self.groupBox_6)
        self.QMultikod.setObjectName("QMultikod")
        self.horizontalLayout_4.addWidget(self.QMultikod)
        self.QMultikodOption = QtGui.QLineEdit(self.groupBox_6)
        self.QMultikodOption.setObjectName("QMultikodOption")
        self.horizontalLayout_4.addWidget(self.QMultikodOption)
        self.gridLayout_2.addWidget(self.groupBox_6, 2, 0, 1, 3)
        self.groupBox = QtGui.QGroupBox(self.tab)
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout = QtGui.QVBoxLayout(self.groupBox)
        self.verticalLayout.setObjectName("verticalLayout")
        self.QCatalogs = QtGui.QPlainTextEdit(self.groupBox)
        self.QCatalogs.setObjectName("QCatalogs")
        self.verticalLayout.addWidget(self.QCatalogs)
        self.QCatalogsOpen = QtGui.QPushButton(self.groupBox)
        self.QCatalogsOpen.setObjectName("QCatalogsOpen")
        self.verticalLayout.addWidget(self.QCatalogsOpen)
        self.gridLayout_2.addWidget(self.groupBox, 3, 0, 1, 1)
        self.groupBox_3 = QtGui.QGroupBox(self.tab)
        self.groupBox_3.setObjectName("groupBox_3")
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.groupBox_3)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.QKeywords = QtGui.QPlainTextEdit(self.groupBox_3)
        self.QKeywords.setObjectName("QKeywords")
        self.verticalLayout_2.addWidget(self.QKeywords)
        self.QKeywordsFill = QtGui.QPushButton(self.groupBox_3)
        self.QKeywordsFill.setObjectName("QKeywordsFill")
        self.verticalLayout_2.addWidget(self.QKeywordsFill)
        self.gridLayout_2.addWidget(self.groupBox_3, 3, 1, 1, 1)
        self.groupBox_4 = QtGui.QGroupBox(self.tab)
        self.groupBox_4.setObjectName("groupBox_4")
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.groupBox_4)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.QTitles = QtGui.QPlainTextEdit(self.groupBox_4)
        self.QTitles.setAutoFillBackground(False)
        self.QTitles.setBackgroundVisible(False)
        self.QTitles.setObjectName("QTitles")
        self.verticalLayout_3.addWidget(self.QTitles)
        self.QTitleWebsite = QtGui.QCheckBox(self.groupBox_4)
        self.QTitleWebsite.setEnabled(True)
        self.QTitleWebsite.setCheckable(True)
        self.QTitleWebsite.setChecked(False)
        self.QTitleWebsite.setObjectName("QTitleWebsite")
        self.verticalLayout_3.addWidget(self.QTitleWebsite)
        self.QTitlesFill = QtGui.QPushButton(self.groupBox_4)
        self.QTitlesFill.setObjectName("QTitlesFill")
        self.verticalLayout_3.addWidget(self.QTitlesFill)
        self.gridLayout_2.addWidget(self.groupBox_4, 3, 2, 1, 1)
        self.QSubmitAll = QtGui.QPushButton(self.tab)
        self.QSubmitAll.setObjectName("QSubmitAll")
        self.gridLayout_2.addWidget(self.QSubmitAll, 4, 0, 1, 1)
        self.QcleanAll = QtGui.QPushButton(self.tab)
        self.QcleanAll.setObjectName("QcleanAll")
        self.gridLayout_2.addWidget(self.QcleanAll, 4, 1, 1, 1)
        self.QCloseAll = QtGui.QPushButton(self.tab)
        self.QCloseAll.setObjectName("QCloseAll")
        self.gridLayout_2.addWidget(self.QCloseAll, 4, 2, 1, 1)
        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QtGui.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.verticalLayout_4 = QtGui.QVBoxLayout(self.tab_2)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.QTexts = QtGui.QPlainTextEdit(self.tab_2)
        self.QTexts.setObjectName("QTexts")
        self.verticalLayout_4.addWidget(self.QTexts)
        self.QTextsFill = QtGui.QPushButton(self.tab_2)
        self.QTextsFill.setObjectName("QTextsFill")
        self.verticalLayout_4.addWidget(self.QTextsFill)
        self.tabWidget.addTab(self.tab_2, "")
        self.tab_3 = QtGui.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.horizontalLayout = QtGui.QHBoxLayout(self.tab_3)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.groupBox_8 = QtGui.QGroupBox(self.tab_3)
        self.groupBox_8.setObjectName("groupBox_8")
        self.verticalLayout_5 = QtGui.QVBoxLayout(self.groupBox_8)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.frame = QtGui.QFrame(self.groupBox_8)
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout_7 = QtGui.QVBoxLayout(self.frame)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.QSearchCategories1 = QtGui.QPushButton(self.frame)
        self.QSearchCategories1.setObjectName("QSearchCategories1")
        self.verticalLayout_7.addWidget(self.QSearchCategories1)
        self.QFetchCategories1 = QtGui.QPushButton(self.frame)
        self.QFetchCategories1.setObjectName("QFetchCategories1")
        self.verticalLayout_7.addWidget(self.QFetchCategories1)
        self.verticalLayout_5.addWidget(self.frame)
        self.QCategories1 = QtGui.QGroupBox(self.groupBox_8)
        self.QCategories1.setObjectName("QCategories1")
        self.vboxlayout = QtGui.QVBoxLayout(self.QCategories1)
        self.vboxlayout.setObjectName("vboxlayout")
        self.QCategoryFilter1 = QtGui.QLineEdit(self.QCategories1)
        self.QCategoryFilter1.setObjectName("QCategoryFilter1")
        self.vboxlayout.addWidget(self.QCategoryFilter1)
        self.scrollArea = QtGui.QScrollArea(self.QCategories1)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents_4 = QtGui.QWidget()
        self.scrollAreaWidgetContents_4.setGeometry(QtCore.QRect(0, 0, 98, 44))
        self.scrollAreaWidgetContents_4.setObjectName("scrollAreaWidgetContents_4")
        self.verticalLayout_9 = QtGui.QVBoxLayout(self.scrollAreaWidgetContents_4)
        self.verticalLayout_9.setObjectName("verticalLayout_9")
        self.frame_2 = QtGui.QFrame(self.scrollAreaWidgetContents_4)
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.QLayout_main = QtGui.QVBoxLayout(self.frame_2)
        self.QLayout_main.setObjectName("QLayout_main")
        self.verticalLayout_9.addWidget(self.frame_2)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_9.addItem(spacerItem)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents_4)
        self.vboxlayout.addWidget(self.scrollArea)
        self.verticalLayout_5.addWidget(self.QCategories1)
        self.horizontalLayout.addWidget(self.groupBox_8)
        self.groupBox_7 = QtGui.QGroupBox(self.tab_3)
        self.groupBox_7.setObjectName("groupBox_7")
        self.verticalLayout_6 = QtGui.QVBoxLayout(self.groupBox_7)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.frame_3 = QtGui.QFrame(self.groupBox_7)
        self.frame_3.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.verticalLayout_8 = QtGui.QVBoxLayout(self.frame_3)
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.QSearchCategories2 = QtGui.QPushButton(self.frame_3)
        self.QSearchCategories2.setObjectName("QSearchCategories2")
        self.verticalLayout_8.addWidget(self.QSearchCategories2)
        self.QFetchCategories2 = QtGui.QPushButton(self.frame_3)
        self.QFetchCategories2.setObjectName("QFetchCategories2")
        self.verticalLayout_8.addWidget(self.QFetchCategories2)
        self.verticalLayout_6.addWidget(self.frame_3)
        self.QCategories2 = QtGui.QGroupBox(self.groupBox_7)
        self.QCategories2.setObjectName("QCategories2")
        self.lay = QtGui.QVBoxLayout(self.QCategories2)
        self.lay.setObjectName("lay")
        self.QCategoryFilter2 = QtGui.QLineEdit(self.QCategories2)
        self.QCategoryFilter2.setObjectName("QCategoryFilter2")
        self.lay.addWidget(self.QCategoryFilter2)
        self.scrollArea_2 = QtGui.QScrollArea(self.QCategories2)
        self.scrollArea_2.setWidgetResizable(True)
        self.scrollArea_2.setObjectName("scrollArea_2")
        self.scrollAreaWidgetContents_5 = QtGui.QWidget()
        self.scrollAreaWidgetContents_5.setGeometry(QtCore.QRect(0, 0, 98, 44))
        self.scrollAreaWidgetContents_5.setObjectName("scrollAreaWidgetContents_5")
        self.verticalLayout_10 = QtGui.QVBoxLayout(self.scrollAreaWidgetContents_5)
        self.verticalLayout_10.setObjectName("verticalLayout_10")
        self.frame_4 = QtGui.QFrame(self.scrollAreaWidgetContents_5)
        self.frame_4.setAutoFillBackground(False)
        self.frame_4.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_4.setObjectName("frame_4")
        self.QLayout_sub = QtGui.QVBoxLayout(self.frame_4)
        self.QLayout_sub.setObjectName("QLayout_sub")
        self.verticalLayout_10.addWidget(self.frame_4)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_10.addItem(spacerItem1)
        self.scrollArea_2.setWidget(self.scrollAreaWidgetContents_5)
        self.lay.addWidget(self.scrollArea_2)
        self.verticalLayout_6.addWidget(self.QCategories2)
        self.horizontalLayout.addWidget(self.groupBox_7)
        self.tabWidget.addTab(self.tab_3, "")
        self.gridLayout.addWidget(self.tabWidget, 0, 0, 1, 1)
        self.QSubmit = QtGui.QPushButton(self.centralwidget)
        self.QSubmit.setObjectName("QSubmit")
        self.gridLayout.addWidget(self.QSubmit, 1, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 662, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "SE-O-Lenium", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_2.setTitle(QtGui.QApplication.translate("MainWindow", "Adres www", None, QtGui.QApplication.UnicodeUTF8))
        self.QwwwFill.setText(QtGui.QApplication.translate("MainWindow", "Uzupelnij", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_5.setTitle(QtGui.QApplication.translate("MainWindow", "E-mail:", None, QtGui.QApplication.UnicodeUTF8))
        self.QMailFill.setText(QtGui.QApplication.translate("MainWindow", "Uzupelnij", None, QtGui.QApplication.UnicodeUTF8))
        self.QMail.setText(QtGui.QApplication.translate("MainWindow", "4gooogle4gooogle@gmail.com", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_6.setTitle(QtGui.QApplication.translate("MainWindow", "Multikod", None, QtGui.QApplication.UnicodeUTF8))
        self.QMultikodFill.setText(QtGui.QApplication.translate("MainWindow", "Uzupelnij", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox.setTitle(QtGui.QApplication.translate("MainWindow", "Adresy katalogów:", None, QtGui.QApplication.UnicodeUTF8))
        self.QCatalogsOpen.setText(QtGui.QApplication.translate("MainWindow", "Otwórz", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_3.setTitle(QtGui.QApplication.translate("MainWindow", "Słowa kluczowe:", None, QtGui.QApplication.UnicodeUTF8))
        self.QKeywordsFill.setText(QtGui.QApplication.translate("MainWindow", "Uzupełnij", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_4.setTitle(QtGui.QApplication.translate("MainWindow", "Tytuły:", None, QtGui.QApplication.UnicodeUTF8))
        self.QTitleWebsite.setText(QtGui.QApplication.translate("MainWindow", "Dołącz adres strony", None, QtGui.QApplication.UnicodeUTF8))
        self.QTitlesFill.setText(QtGui.QApplication.translate("MainWindow", "Uzupełnij", None, QtGui.QApplication.UnicodeUTF8))
        self.QSubmitAll.setText(QtGui.QApplication.translate("MainWindow", "Uzupelnij wszystko", None, QtGui.QApplication.UnicodeUTF8))
        self.QcleanAll.setText(QtGui.QApplication.translate("MainWindow", "Wyczyść wszystko", None, QtGui.QApplication.UnicodeUTF8))
        self.QCloseAll.setText(QtGui.QApplication.translate("MainWindow", "Zamknij wszystko", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QtGui.QApplication.translate("MainWindow", "Ogolne", None, QtGui.QApplication.UnicodeUTF8))
        self.QTextsFill.setText(QtGui.QApplication.translate("MainWindow", "Wypelnij", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QtGui.QApplication.translate("MainWindow", "Teksty", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_8.setTitle(QtGui.QApplication.translate("MainWindow", "Pierwszy poziom kategorii", None, QtGui.QApplication.UnicodeUTF8))
        self.QSearchCategories1.setText(QtGui.QApplication.translate("MainWindow", "Zaznacz kategorie", None, QtGui.QApplication.UnicodeUTF8))
        self.QFetchCategories1.setText(QtGui.QApplication.translate("MainWindow", "Pobierz kategorie", None, QtGui.QApplication.UnicodeUTF8))
        self.QCategories1.setTitle(QtGui.QApplication.translate("MainWindow", "Wykryte kategorie", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_7.setTitle(QtGui.QApplication.translate("MainWindow", "Drugi poziom kategorii", None, QtGui.QApplication.UnicodeUTF8))
        self.QSearchCategories2.setText(QtGui.QApplication.translate("MainWindow", "Zaznacz kategorie", None, QtGui.QApplication.UnicodeUTF8))
        self.QFetchCategories2.setText(QtGui.QApplication.translate("MainWindow", "Pobierz kategorie", None, QtGui.QApplication.UnicodeUTF8))
        self.QCategories2.setTitle(QtGui.QApplication.translate("MainWindow", "Wykryte kategorie", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), QtGui.QApplication.translate("MainWindow", "Kategorie", None, QtGui.QApplication.UnicodeUTF8))
        self.QSubmit.setText(QtGui.QApplication.translate("MainWindow", "Wyślij", None, QtGui.QApplication.UnicodeUTF8))

